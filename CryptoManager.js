const crypto = require('crypto');
const fs = require('fs');

const algorithm = "aes-256-cbc";

const filePath = './files/internal/filesInfo.json';

const saveEncryptionInfo = (fileName, isEncrypted) => {
    const isFileExist = fs.existsSync(filePath);

    if (!isFileExist) {
        fs.mkdirSync('./files/internal');
        fs.writeFileSync(filePath, "[]");
    }

    const info = JSON.parse(fs.readFileSync(filePath, 'utf-8'));

    const index = info.findIndex(({ name }) => name === fileName);

    if (index !== -1) {
        info[index].isEncrypted = isEncrypted;
    } else {
        info.push({
            name: fileName,
            isEncrypted
        });
    }

    fs.writeFileSync(filePath, JSON.stringify(info));
}

const isEncrypted = (fileName) => {
    try {
        const info = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
        const index = info.findIndex(({ name }) => name === fileName);

        if (index === -1) {
            return false;
        }

        return info[index].isEncrypted;
    } catch (error) {
        return false;
    }
}

const encryptContent = (password, content) => {
    const key = crypto.createHash('sha256').update(String(password)).digest('base64').substring(0, 32);
    const vector = crypto.createHash('sha256').update(String(password)).digest('base64').substring(0, 16);

    const cipher = crypto.createCipheriv(algorithm, key, vector);

    let encryptedData = cipher.update(content, "utf-8", "hex");
    encryptedData += cipher.final("hex");

    return encryptedData;
};

const decryptContent = (password, content) => {
    const key = crypto.createHash('sha256').update(String(password)).digest('base64').substring(0, 32);
    const vector = crypto.createHash('sha256').update(String(password)).digest('base64').substring(0, 16);

    const decipher = crypto.createDecipheriv(algorithm, key, vector);

    let decryptedData = decipher.update(content, "hex", "utf-8");
    decryptedData += decipher.final("utf8");

    return decryptedData;
}

module.exports = {
    saveEncryptionInfo,
    decryptContent,
    encryptContent,
    isEncrypted
};
