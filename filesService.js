const fs = require('fs');
const path = require('path');

const CryptoManager = require('./CryptoManager');


function createFile(req, res) {
  const { content, filename, password } = req.body;

  const fileName = filename?.trim();

  if (!content) {
    res.status(400).send({
      message: "Please specify 'content' parameter"
    });
    return;
  }

  if (!fileName) {
    res.status(400).send({
      message: "Please specify 'filename' parameter"
    });
    return;
  }

  const filePath = path.resolve('files', fileName);

  if (fs.existsSync(filePath)) {
    res.status(400).send({
      message: 'File already exists'
    });
    return;
  }

  fs.writeFileSync(
    filePath,
    password
      ? CryptoManager.encryptContent(password, content)
      : content
  );
  CryptoManager.saveEncryptionInfo(fileName, !!password);

  res.status(200)
    .send({ message: "File created successfully" });
}

function getFiles(req, res) {
  const files = fs.readdirSync('./files', {
    withFileTypes: true
  });

  if (files.length < 1) {
    res.status(400).send({
      message: "No files found"
    });
    return;
  }

  res.status(200).send({
    message: "Success",
    files: files.filter((file) => file.isFile()).map(({ name }) => name)
  });
}

const getFile = (req, res) => {
  const { filename: fileName } = req.params;
  const { password } = req.query;
  const filePath = `./files/${fileName}`;

  const isFileExist = fs.existsSync(filePath);

  if (!isFileExist) {
    res.status(400).send({
      message: `No file with ${fileName} filename found`
    });

    return;
  }

  const isEncrypted = CryptoManager.isEncrypted(fileName);

  if (isEncrypted && !password) {
    res.status(401).send({
      message: "Unathorized access"
    });
    return;
  }

  let content = fs.readFileSync(filePath, 'utf8');

  if (isEncrypted) {
    try {
      content = CryptoManager.decryptContent(password, content);
    } catch (error) {
      if (error.code === 'ERR_OSSL_EVP_BAD_DECRYPT') {
        res.status(401).send({
          message: "Unathorized access"
        });
        return;
      }

      throw error;
    }
  }

  const { birthtime } = fs.statSync(filePath);

  res.status(200).send({
    message: "Success",
    filename: fileName,
    content,
    extension: path.extname(fileName).replace('.', ''),
    uploadedDate: birthtime
  });
}

function editFile(req, res) {
  const { filename: fileName } = req.params;
  const { content } = req.body;
  const { password } = req.query;

  const isEncrypted = CryptoManager.isEncrypted(fileName);
  const filePath = `./files/${fileName}`;

  if (isEncrypted && !password) {
    res.status(401).send({
      message: "Unathorized access"
    });
    return;
  }

  if (isEncrypted) {
    try {
      CryptoManager.decryptContent(password, fs.readFileSync(filePath, 'utf8'));
    } catch (error) {
      if (error.code === 'ERR_OSSL_EVP_BAD_DECRYPT') {
        res.status(401).send({
          message: "Unathorized access"
        });
        return;
      }

      throw error;
    }
  }

  if (!content) {
    res.status(400).send({
      message: "Please specify 'content' parameter"
    });
    return;
  }


  fs.writeFileSync(
    filePath,
    isEncrypted
      ? CryptoManager.encryptContent(password, content)
      : content
  )

  res.status(200)
    .send({ message: "File modified successfully" });
}

function deleteFile(req, res) {
  const { filename: fileName } = req.params;
  const { password } = req.query;

  const filePath = `./files/${fileName}`;

  const isEncrypted = CryptoManager.isEncrypted(fileName);

  if (isEncrypted && !password) {
    res.status(401).send({
      message: "Unathorized access"
    });
    return;
  }

  if (isEncrypted) {
    try {
      CryptoManager.decryptContent(password, fs.readFileSync(filePath, 'utf8'));
    } catch (error) {
      if (error.code === 'ERR_OSSL_EVP_BAD_DECRYPT') {
        res.status(401).send({
          message: "Unathorized access"
        });
        return;
      }

      throw error;
    }
  }

  const isFileExist = fs.existsSync(filePath);

  if (!isFileExist) {
    res.status(400).send({
      message: `Nothing to delete. No file ${fileName} is found`
    });

    return;
  }

  fs.unlinkSync(filePath);

  res.status(200)
    .send({ message: "File delete successfully" });

}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
